<?php 
/*******************************
  THEME OPTIONS PAGE
********************************/

add_action('admin_menu', 'corise_theme_page');
function corise_theme_page ()
{
	if ( count($_POST) > 0 && isset($_POST['corise_settings']) )
	{
		$options = array ('page_info', 'home_info', 'file_upload','banner_image', 'banner_link', 'contact_email','contact_text', 'logo_link','cufon','linkedin_link','twitter_user','latest_tweet','facebook_link','keywords','description','analytics','copyright','home_box1','home_box1_link','home_box2','home_box2_link','home_box3','home_box3_link','blurb_enable','blurb_text','blurb_link','blurb_page', 'footer_actions','actions_hide','portfolio','blog','slider', 'contact_mail');
		
		foreach ( $options as $opt )
		{
			delete_option ( 'corise_'.$opt, $_POST[$opt] );
			add_option ( 'corise_'.$opt, $_POST[$opt] );	
		}			
		 
	}
	add_menu_page(__('Theme Options'), __('Theme Options'), 'edit_themes', basename(__FILE__), 'corise_settings');
	add_submenu_page(__('Theme Options'), __('Theme Options'), 'edit_themes', basename(__FILE__), 'corise_settings');
}


//file upload
function my_admin_scripts() {
	wp_enqueue_media();
	wp_register_script( 'corise-js', get_stylesheet_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );
	wp_enqueue_script('corise-js');
}
 

add_action('admin_print_scripts', 'my_admin_scripts');


function corise_settings()
{?>
<div class="wrap">
	<h2>Theme Options Panel</h2>
	
<form method="post" action="">

	<fieldset style="border:1px solid #ddd; padding:10px; margin-top:20px;">
	<legend style="margin-left:5px; padding:0 5px;color:#2481C6; text-transform:uppercase;"><strong>Genereal settings</strong></legend>
	<table class="form-table">
		<!-- General settings -->
		
		 <tr valign="top">
			<th scope="row"><label for="portfolio">Select category.</label></th>
			<td>
				<?php wp_dropdown_categories("name=portfolio&hide_empty=0&show_option_none=".__('- Select -')."&selected=" .get_option('corise_portfolio')); ?>
			</td>
		</tr>
	</table>
	</fieldset>
	
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="Save Changes" />
		<input type="hidden" name="corise_settings" value="save" style="display:none;" />
		</p>
	
	
	
		
		<fieldset style="border:1px solid #ddd; padding:10px; margin-top:20px;">
	<legend style="margin-left:5px; padding:0 5px;color:#2481C6; text-transform:uppercase;"><strong>Additional settings</strong></legend>
	<table class="form-table">
		<!-- Homepage Boxes 1 -->
		<tr>
			<th colspan="2"><strong>Heading</strong></th>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="slider">Select Page</label></th>
			<td>
				<?php wp_dropdown_pages("name=slider&show_option_none=".__('- Select -')."&selected=" .get_option('corise_slider')); ?>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="splash_link">Select another page</label></th>
			<td>
				<?php $posts = get_posts(array('post_type'=> 'albums', 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
				$selected = get_option('corise_splash_link');
				echo '<select name="splash_link" id="splash_link">';
				echo '<option value = "" >Select album </option>';
				foreach ($posts as $post) {
					echo '<option value="', get_permalink($post->ID), '"', $selected == get_permalink($post->ID) ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
				}
				echo '</select>'; ?>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">File upload.</th>
			<td><label for="upload_image2">
				<img class="custom_media_image" src="<?php echo (get_option('corise_file_upload')); ?>" style="max-width:100px; float:left; margin: 0px     10px 0px 0px; display:inline-block;" />

				<input class="custom_media_url" type="text" name="file_upload" size="40" value="<?php echo (get_option('corise_file_upload')); ?>">
				<input class="custom_media_upload" type="button" value="Upload Image" />
				<input class="custom_media_id" type="hidden" name="file_upload_id" value="">
				<br />Enter the URL or upload an image.
				
			</label></td>
		</tr>
		
		<tr valign="top">
			<th scope="row"><label for="blurb_text">Text area.</label></th>
			<td>
				<textarea name="blurb_text" id="blurb_text" rows="3" cols="70" style="font-size:11px;"><?php echo stripslashes(get_option('corise_blurb_text')); ?></textarea>
			</td>
		</tr>

		
	</table>
	
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="Save Changes" />
		<input type="hidden" name="corise_settings" value="save" style="display:none;" />
		</p>
	
    
	
	
        
     
</form>
</div>
<?php }?>