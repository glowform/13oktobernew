<?php
/*
 Template Name: Landing Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content">

						<div id="main" class="m-all t-all d-all cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<section class="entry-content cf" itemprop="articleBody">

									<div class="top-section">
										<video id="vid" loop muted autoplay preload poster="<?php echo get_template_directory_uri(); ?>/library/images/top_bg.jpg" class="fullscreen-bg-video">
										        <source src="<?php echo get_template_directory_uri(); ?>/library/video/video_bg.webm" type="video/webm"; codecs=vp8,vorbis>
										        <source src="<?php echo get_template_directory_uri(); ?>/library/video/video_bg.mp4">
										        <!-- <source src="video/big_buck_bunny.ogv" type="video/ogg"> -->
										</video>
									
										<div class="overlay1"></div>
										<div class="overlay2"></div>
										<div class="wrap cf">
											 
											<div class="logos">
												<div class="logo-5"><img src="<?php echo get_template_directory_uri(); ?>/library/images/5logo.png" alt=""></div>
												<div>
													<h3>Premiere <br>den 2. april kl. 21.00</h3>
													<img class="mainlogo" src="<?php echo get_template_directory_uri(); ?>/library/images/mainlogo.png" alt="">
												</div>
											</div>

											<div class="top-inner">

												<div class="video">
													<a id="trailer" href="#" class="hvr-rectangle-out"><img src="<?php echo get_template_directory_uri(); ?>/library/images/video_trailer2.png" alt=""></a>
												</div>

												<h3>Ny dansk dramaserie</h3>
												<p>En regnvåd oktobermorgen får 15 danskere deres liv vendt på hovedet, da den metro, de har søgt ly i, bliver kapret af ukendte gerningsmænd, der truer med at likvidere ét efter ét af de 15 uskyldige danske gidsler, hvis de ikke får deres krav opfyldt. </p>

												<p>Det bliver startskuddet på en historisk gidselaktion, som i løbet af én enkelt uge ændrer Danmark for altid. Det officielle Danmark sættes på den anden ende. Hos politikerne diskuteres det, om man bør forhandle med terrorister. I pressen diskuteres det, hvor langt man må gå i forhold til at skrive om gidslerne og deres familier. Almindelige danskere tænker på, at det kunne have været dem. Hos gidslerne veksler man mellem oprørstanker, forhandling og apati, men én tanke går igen: Skal jeg dø nu?
												</p>
											</div>
										</div>
									</div>

									<div class="middle-section parallax-window" data-parallax="scroll"  data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/bg_img1.jpg" data-natural-width="1920" data-natural-height="2004" data-position-y="0px">
										<div class="wrap cf">
											<h2>Persongalleri</h2>

											<div class="cf">

												<div class="m-all t-1of2 d-1of2 left">
													<ul class="main-cast">
														<li><a class="hvr-rectangle-out" id="philip" href="#"><img  src="<?php echo get_template_directory_uri(); ?>/library/images/philip.png" alt="">
														<p>Philip Nørgaard<span>leder af pets nyoprettede terror task force</span><span>JOHANNES LASSEN</span></p></a></li>
													</ul>

													<a class="d-banner" target="_blank" href="http://www.dplay.dk/"><img src="<?php echo get_template_directory_uri(); ?>/library/images/dplaybanner.png" alt=""></a>
												</div>

												<div class="m-all t-1of2 d-1of2 right last-col">
													<ul class="main-cast">
														<li><a class="hvr-rectangle-out" id="naja" href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/images/naja.png" alt="">
														<p>Naja Toft<span>Journalist</span><span>PAPRIKA STEEN</span></p></a></li>
														
														<li><a class="hvr-rectangle-out" id="louise" href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/images/louise.png" alt="">
														<p>Louise Falck<span>Forhandler i politiets forhandlerteam </span><span>SARA HJORT DITLEVSEN</span></p></a></li>
														
														<li><a class="hvr-rectangle-out" id="adel" href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/images/adel.png" alt="">
														<p>adel Rasul<span>gidsel</span><span>DAR SALIM</span></p></a></li>
														
														
													</ul>

													<a class="m-banner" href="http://www.dplay.dk/"><img src="<?php echo get_template_directory_uri(); ?>/library/images/dplaybanner.png" alt=""></a>
												</div>

											</div>
											
									

											<div class="cf cast-section">
											
												<ul class="cast-list">
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/denise.png" alt=""><p>DENISE, GIDSEL<span>Sus Wilkins</span></p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/leon.png" alt=""><p>LEON, GIDSEL<span>Tommy Kenter</span></p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/marie.png" alt=""><p>MARIE, GIDSEL <span>Alba August</span></p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/ricco.png" alt=""><p>RICCO, GIDSEL<span>Anders Nyborg</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/joachim.png" alt=""><p>JOACHIM, GIDSEl<span>Michael Asmussen</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/bodil.png" alt=""><p>bodil, gidsel<span>Lane Lind</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/silas.png" alt=""><p>silas, gidsel<span>Allan Hyde</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/henning.png" alt=""><p>HENNING NØRGAARD, tidligere forsvars-chef<span>Henning Jensen</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/sp.png" alt=""><p>S.P., Efterforsker<span>Alexandre willaume</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/peder.png" alt=""><p>SIMON CLAUSEN, Efterforsker<span>peder pedersen</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/daniel.png" alt=""><p>DANIEL CRAMER, Leder af politiets aktionsstyrke<span>Kenneth M. Christensen</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/esben.png" alt=""><p>ESBEN GARNOV, Leder af politiets forhandler-team<span>Esben Dalgaard Andersen</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/hans.png" alt=""><p>HANS HEjNDORF, Chef for PET<span>Flemming Enevold</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/anne.png" alt=""><p>ANNE GORNSTEIN, Rigspoliti-chef<span>Cecilie Stenspil</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/palle.png" alt=""><p>PALLE WULFF, Forsvars-chef<span>Henrik Prip</span> </p></li>
													<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/jonas.png" alt=""><p>JONAS, pårørende til gidslet, Leon<span>Jacob Lohmann</span> </p></li>
											
												
												</ul>

											</div>
										</div>
									</div>

									<div class="parallax-window pr2" data-parallax="scroll"  data-image-src="<?php echo get_template_directory_uri(); ?>/library/images/bg_img2.jpg" data-natural-width="1920" data-natural-height="1200" >
										<div class="bottom-section wrap cf">
											<h2>Holdet bag serien</h2>
											<div class="m-all t-all d-1of2">
												<div class="accordion">
													<h4>Kasper Barfoed</h4>
													<span>(hovedforfatter & konceptuerende instruktør)</span>
													<p>Kasper Barfoed har bl.a. instrueret spilleflmene Sommeren ’92, Kandidaten, Tempelridderens Skat og den amerikanske film The Numbers Station med John Cusack. Sommeren ’92 var han også manuskriptforfatter på i samarbejde med Anders August. Kasper Barfoed har instrueret flere afsnit af tv-serien Den Som Dræber og var konceptuerende instruktør på anden sæson af tv-serien Dicte.</p>
												</div>
												<div class="accordion">
													<h4>Morten Kjems Hytten Juhl</h4>
													<span>(producer)</span>
													<p>Morten har senest vundet prisen for bedste film med den svensk/danske spillefilm Jætten, instrueret af Johannes Nyholm (2016), ved den svenske prisuddeling af Guldbaggen. Tidligere titler omfatter Mads Matthiesens 10 timer til Paradis (bedste instruktør ved Sundance i 2012), Fenar Ahmads Ækte Vare (London Film Festival, 2014). Han har desuden været producer på ultra lavbudget sketch-serien Turbomodul (2014) for TV2 Zulu og tv-dramaserien Lulu & Leon, skabt af Lolita Bellstar og Jens Dahl for TV3, 2009. </p>
												</div>
												<div class="accordion">
													<h4>Adam Price</h4>
													<span>(executive producer)</span>
													<p>Adam Price er Executive Producer ved og medejer af SAM Productions ApS, og har senest stået bag den prisvindende og internationalt anerkendte tv-serie Borgen. Med serier som Anna Pihl, Nikolaj og Julie og Taxa bag sig er Price et velrenommeret navn indenfor international tv-serieproduktion. Adam Price er desuden forfatteren bag flere anmelderroste teaterforestillinger og vært på tv-programmet Spise med Price.</p>
												</div>
											
											</div>
											
											<div class="m-all t-all d-1of2">
												
												<div class="accordion">
													<h4>Søren Sveistrup</h4>
													<span>(executive producer)</span>
													<p>Søren Sveistrup er Executive Producer ved og medejer af SAM Productions ApS. Sveistrup er en internationalt anerkendt forfatter og er blandt andet manden bag tre sæsoner af den prisvindende tv-serie Forbrydelsen. På den amerikanske remake af serien fungerede Sveistrup som Executive Producer. Søren Sveistrup har tidligere fungeret som co-creator og forfatter af Nikolaj og Julie og har skrevet store dele af Hotellet og Taxa.</p>
												</div>
												<div class="accordion">
													<h4>META LOUISE FOLDAGER SØRENSEN</h4>
													<span>(CEO &amp; Executive producer)</span>
													<p>Meta Louise Foldager Sørensen er Executive Producer ved og medejer af SAM Productions ApS. Foldager er produceren bag nogle af Danmark største flmproduktioner - heriblandt Antichrist, Melancholia og En Kongelig Affære. Meta Louise Foldager Sørensen er desuden indehaver af produktionsselskabet Meta Film, hvor hun senest har produceret Sommeren ’92, I Dine Hænder og Ækte Vare.</p>
												</div>
											</div>
											
										</div>

										<div class="wrap cf footer">
											<div class="m-all t-1of2 d-1of2">
												<p>Discovery networks<br>
												H. C. Andersens Boulevard 1<br>
												DK-1553 Copenhagen V<br>
												<a href="tel:004570101010">+45 70 10 10 10</a><br><br>
												Manager PR &amp; Digital:<br>
												<a href="mailto:lena_boegild@discovery.com">lena_boegild@discovery.com</a><br><br>
												<a href="http://discoverynetworks.dk/presse">Gå til pressesite</a></p>
											</div>

											<div class="m-all t-1of2 d-1of2 last-col">
												<img src="<?php echo get_template_directory_uri(); ?>/library/images/footerlogos.png" alt="">
											</div>
										</div>
									</div>
									
								</section>



							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'corisetheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'corisetheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'corisetheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						

				</div>

			</div>


<?php get_footer(); ?>
